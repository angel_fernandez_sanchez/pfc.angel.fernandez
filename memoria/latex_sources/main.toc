\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {vi}}{chapter*.4}
\contentsline {chapter}{\'{I}ndice general}{\es@scroman {vii}}{chapter*.5}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xi}}{chapter*.6}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xii}}{chapter*.7}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xiv}}{chapter*.8}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xv}}{chapter*.9}
\contentsline {chapter}{Agradecimientos}{\es@scroman {xvii}}{chapter*.10}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Estructura del documento}{2}{section.1.1}
\contentsline {chapter}{\numberline {2}Antecedentes}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Cloud Computing}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Caracter\IeC {\'\i }sticas}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Modelos de servicio}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Modelos de despliegue}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Etapas en la adopci\IeC {\'o}n del modelo Cloud Computing}{7}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}CloudStack}{8}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Open Stack}{9}{subsection.2.1.6}
\contentsline {section}{\numberline {2.2}Disponibilidad de servicios}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}Monitorizaci\IeC {\'o}n de sistemas}{12}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Definiciones}{12}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Clasificaci\IeC {\'o}n de los sistemas de monitorizaci\IeC {\'o}n}{15}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Monitorizaci\IeC {\'o}n en entornos Cloud}{17}{section.2.4}
\contentsline {section}{\numberline {2.5}An\IeC {\'a}lisis de algunas herramientas de monitorizaci\IeC {\'o}n no comerciales}{17}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Nagios}{17}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Sensu}{20}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}Monasca}{22}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Monasca-API}{24}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}Monasca-agent}{24}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}An\IeC {\'a}lisis de ventajas e inconvenientes de la soluci\IeC {\'o}n Monasca}{24}{subsection.2.6.3}
\contentsline {chapter}{\numberline {3}Objetivos}{28}{chapter.3}
\contentsline {section}{\numberline {3.1}Objetivo general}{28}{section.3.1}
\contentsline {section}{\numberline {3.2}Objetivos espec\IeC {\'\i }ficos}{28}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Objetivo 1}{28}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Objetivo 2}{28}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Objetivo 3}{28}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Objetivo 4}{29}{subsection.3.2.4}
\contentsline {chapter}{\numberline {4}Metodolog\IeC {\'\i }a de Trabajo y Herramientas}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}Metodolog\IeC {\'\i }a de Trabajo}{30}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Prototipado Incremental}{30}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}Recursos utilizados en el desarrollo del proyecto}{31}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Hardware}{31}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Lenguajes de programaci\IeC {\'o}n y scripting}{32}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Plataformas software}{32}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Sistemas operativos}{32}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Editores de texto}{32}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Documentaci\IeC {\'o}n}{32}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}Otras Herramientas}{32}{subsection.4.2.7}
\contentsline {chapter}{\numberline {5}Desarrollo}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}Iteraci\IeC {\'o}n 0: Instalaci\IeC {\'o}n del entorno DevStack con Monasca}{33}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Descarga del repositorio DevStack}{34}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Configuraci\IeC {\'o}n del entorno}{34}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Arranque del entorno}{36}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Creaci\IeC {\'o}n de la Base de Datos de Monitorizaci\IeC {\'o}n}{36}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Preparaci\IeC {\'o}n del acceso SSH a las instancias}{37}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Aprovisionamiento de im\IeC {\'a}genes}{38}{subsection.5.1.6}
\contentsline {section}{\numberline {5.2}Iteraci\IeC {\'o}n 1: Creaci\IeC {\'o}n de un script gen\IeC {\'e}rico para instalaci\IeC {\'o}n y configuraci\IeC {\'o}n del agente de monitorizaci\IeC {\'o}n}{39}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Requisitos}{39}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}An\IeC {\'a}lisis}{39}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Desarrollo}{42}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Pruebas}{43}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}Iteraci\IeC {\'o}n 2: Automatizaci\IeC {\'o}n de la instalaci\IeC {\'o}n del agente Monasca}{44}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Requisitos}{44}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}An\IeC {\'a}lisis}{44}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Desarrollo}{46}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Pruebas}{47}{subsection.5.3.4}
\contentsline {section}{\numberline {5.4}Iteraci\IeC {\'o}n 3: Notificaci\IeC {\'o}n de resultados en la interfaz de administraci\IeC {\'o}n}{48}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Requisitos}{48}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}An\IeC {\'a}lisis}{49}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Desarrollo}{51}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Pruebas}{52}{subsection.5.4.4}
\contentsline {chapter}{\numberline {6}Resultados}{53}{chapter.6}
\contentsline {section}{\numberline {6.1}Aplicaci\IeC {\'o}n de la soluci\IeC {\'o}n}{53}{section.6.1}
\contentsline {section}{\numberline {6.2}Estimaci\IeC {\'o}n de costes del proyecto}{55}{section.6.2}
\contentsline {section}{\numberline {6.3}Repositorio}{56}{section.6.3}
\contentsline {chapter}{\numberline {7}Conclusiones y trabajo futuro}{57}{chapter.7}
\contentsline {section}{\numberline {7.1}Objetivos alcanzados}{57}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Objetivos directos}{57}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Objetivos indirectos}{57}{subsection.7.1.2}
\contentsline {section}{\numberline {7.2}Trabajo futuro}{58}{section.7.2}
\contentsline {chapter}{\numberline {A}Casos de prueba}{60}{appendix.Alph1}
\contentsline {section}{\numberline {A.1}Casos de prueba de la iteraci\IeC {\'o}n 1}{60}{section.Alph1.1}
\contentsline {section}{\numberline {A.2}Casos de prueba de la iteraci\IeC {\'o}n 2}{61}{section.Alph1.2}
\contentsline {section}{\numberline {A.3}Casos de prueba de la iteraci\IeC {\'o}n 3}{62}{section.Alph1.3}
\contentsline {chapter}{\numberline {B}Plantillas de Heat}{63}{appendix.Alph2}
\contentsline {section}{\numberline {B.1}Plantilla de ejemplo del recurso OS::Nova::Server}{63}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}Plantilla de prueba del recurso OS::Nova::Server::Monitored }{65}{section.Alph2.2}
\contentsline {chapter}{\numberline {C}Metricas obtenidas por el Agente Monasca}{66}{appendix.Alph3}
\contentsline {chapter}{\numberline {D}Creaci\IeC {\'o}n de una pila con Heat}{68}{appendix.Alph4}
\contentsline {chapter}{\numberline {E}Creaci\IeC {\'o}n de alertas con Monasca}{70}{appendix.Alph5}
\contentsline {chapter}{\numberline {F}Visualizaci\IeC {\'o}n de gr\IeC {\'a}ficas con Grafana}{74}{appendix.Alph6}
\contentsline {chapter}{Bibliograf\'{\i }a}{80}{appendix*.28}
