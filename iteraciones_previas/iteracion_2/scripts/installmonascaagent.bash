#!/bin/bash
#AUTHOR ANGEL FERNANDEZ SANCHEZ
#THE FOLLOWING VARIABLES MUST BE EDITED DEPENDING ON ACTUAL OPEN STACK AND MONASCA INSTALLATION
MON_API_USER=monasca-agent
MON_API_PASSWORD=password
KEYSTONE_URL=http://192.168.80.128:35357/v3/
MONASCA_API_URL=http://192.168.80.128:8070/v2.0
MONASCA_PROJECT_NAME=mini-mon
#
LOG_FILE=/tmp/monasca_agent_install.log
MONASCA_SUPERVISOR_LOGFILE=/var/log/monasca/agent/supervisord.log

MONASCA_SETUP_PATH=/usr/local/bin/monasca-setup
MONASCA_SETUP_PATH_RHL=/usr/bin/monasca-setup

install_debian_based_prerreqs(){
 sudo apt-get update
 sudo apt-get install python-dev python-pip git libxml2-dev libxslt1-dev zlib1g-dev build-essential -y
}

install_centos7_prerreqs(){
 sudo  yum -y install sysstat python-devel libxml2-devel libxslt-devel redhat-rpm-config gcc >> $LOG_FILE
 sudo  curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" >> $LOG_FILE
 sudo  python get-pip.py
}

install_fedora_prerreqs(){
 echo "fedora prerreqs"
 sudo dnf -y install sysstat python-pip python-devel libxml2-devel libxslt-devel redhat-rpm-config gcc
 echo "fin fedora prerreqs"
}

install_python_reqs(){
 sudo pip install --upgrade pip
 sudo pip install "requests>=2.9.1"
 sudo pip install "psutil>=3.4.2"
 echo "python rq"
}

install_agent(){
 sudo pip install --upgrade monasca-agent
}

configure_agent(){
 sudo $MONASCA_SETUP_PATH -u $MON_API_USER -p $MON_API_PASSWORD --keystone_url $KEYSTONE_URL --monasca_url $MONASCA_API_URL  --project_name $MONASCA_PROJECT_NAME
}

# GET OS DISTRIBUTION AND VERSION
OS=$(cat /etc/os-release | grep ID= | grep -v VERSION_ID | grep -v VARIANT_ID | tr '='  ' ' | awk {'print $2'})
VERSION=$(cat /etc/os-release | grep VERSION_ID | tr '='  ' ' | awk {'print $2'})
ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')

OS=$(echo $OS | tr -d '"')
VERSION=$(echo $VERSION | tr -d '"')

echo $OS $VERSION $ARCH

supported=0

if [ "$OS" == "debian" ] && [ "$VERSION" == "8" ]; then
 supported=1
 install_debian_based_prerreqs
fi

if [ "$OS" == "ubuntu" ] && [ "$VERSION" == "16.04" ]; then
 supported=1
 install_debian_based_prerreqs
fi

if [ "$OS" == "ubuntu" ] && [ "$VERSION" == "14.04" ]; then
 supported=1
 install_debian_based_prerreqs
fi

if [ $OS = "centos" ] && [ $VERSION = "7" ]; then
 supported=1
 MONASCA_SETUP_PATH=$MONASCA_SETUP_PATH_RHL
 install_centos7_prerreqs
 sudo pip install "setuptools >=1.4"
fi

if [ "$OS" = "fedora" ] && [ "$VERSION" = "24" ]; then
 supported=1
 MONASCA_SETUP_PATH=$MONASCA_SETUP_PATH_RHL
 install_fedora_prerreqs
fi

if [ $supported -eq 0 ];then
  echo "Unsupported version or release: $OS $VERSION" >> $LOG_FILE
  exit -1
fi

 install_python_reqs
 install_agent

 # check agent installed
if [ -f $MONASCA_SETUP_PATH  ] || [ -f $MONASCA_SETUP_PATH_CENTOS ]; then
  configure_agent
  sleep 5
  # check monasca agent processes (collector, statsd and forwarder) running
  scollector=$(cat $MONASCA_SUPERVISOR_LOGFILE |grep -c "collector entered RUNNING state")
  sstatsd=$(cat $MONASCA_SUPERVISOR_LOGFILE |grep -c "statsd entered RUNNING state")
  sforwarder=$(cat $MONASCA_SUPERVISOR_LOGFILE |grep -c "forwarder entered RUNNING state")
  if [ $scollector -ne 0 ] && [ $sstatsd -ne 0 ] && [ $sforwarder -ne 0 ]; then
     echo "Monasca Agent installed, configured and running." >> $LOG_FILE
  else
     echo "Monasca Agent could'nt be configured and started properly." >> $LOG_FILE
  fi
else
 echo "Monasca Agent could'nt be installed properly." >> $LOG_FILE
fi