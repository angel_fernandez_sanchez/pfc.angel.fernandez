# Proyecto de fin de carrera

## Creación de una solución basada en Open Source para monitorización automática de platafomas IaaS ##

### Angel Fernández Sánchez ###

### Escuela Superior de Informática de Ciudad Real ###

### Septiembre 2016 ###


Contenido de los directorios:



/iteraciones_previas : contenido de los elementos desarrollados en las iteraciones del modelo incremental, previas al prototipo definitivo.

                    /iteracion_0 : iteración de construcción del entorno Dev Stack, se incluye el archivo local.conf utilizado para construir el entorno en el directorio /util.

                    /iteracion_1 : iteración de desarrollo del script genérico de instalación del agente monasca para varias distribuciones y versiones soportadas del Sistema Operativo GNU/Linux. El script desarrollado (installmonascaagent.sh) se encuentra dentro del directorio /scripts.

                    /iteracion_2 : iteración de automatización del script desarrollado en la iteración 1 mediante plantillas HOT del componente de orquestación de Open Stack (Heat). Contiene varias subcarpetas:

                                   /heat_templates: contiene la plantilla del tipo de recurso OS::Nova::Server::Monitored
           
                                   /heat_environment: contiene el archivo de entorno Heat necesario para registrar el tipo de recurso OS::Nova::Server::Monitored en la plataforma Open Stack

                                   /tests: contiene las plantillas HOT utilizadas en las pruebas de validación de la última fase del modelo incremental.

/prototipo_final : elementos desarrollados en la última iteración del modelo incremental, que añade notificaciones al usuario del proceso de instalación del agente Monasca que puede visualizar en la interfaz gráfica de usuario de Open Stack (Horizon)

                    /heat_templates: contiene la plantilla del tipo de recurso OS::Nova::Server::Monitored
           
                    /heat_environment: contiene el archivo de entorno Heat necesario para registrar el tipo de recurso OS::Nova::Server::Monitored en la plataforma Open Stack

                    /tests: contiene las plantillas HOT utilizadas en las pruebas de validación de la última fase del modelo incremental.

                    /examples: contiene plantillas de ejemplo adicionales de uso del recurso OS::Nova::Server::Monitored, que crean varias instancias del recurso a la vez.

/memoria : contiene la memoria del pfc en formato pdf. También se incluyen en el directorio latex_sources los archivos fuente y las imágenes utilizados para generar la memoria.