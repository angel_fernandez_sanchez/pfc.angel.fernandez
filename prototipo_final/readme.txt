cp heat_environments/env_os_nova_server_monitored.yaml /etc/heat/environment.d
cp heat_templates/OS_Nova_Server_Monitored.yaml /etc/heat/templates

Configure the following variables in OS_Nova_Server_Monitored.yaml depending on your current Monasca and Open Stack installations:

MON_API_USER=monasca-agent
MON_API_PASSWORD=password
KEYSTONE_URL=http://192.168.80.170:35357/v3/
MONASCA_API_URL=http://192.168.80.170:8070/v2.0
MONASCA_PROJECT_NAME=mini-mon


---RESTART HEAT SERVICES.
 h-eng 
 h-api 
 h-api-cfn 
 h-api-cw
